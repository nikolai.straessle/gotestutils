package gotestutils

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/random"
	"github.com/spf13/viper"
	"io"
	"net/http"
	"net/http/httptest"
)

var currentMock sqlmock.Sqlmock

var Tables []string

type Mock struct {
	QueryType   string
	Query       string
	ReturnError bool
	ResultRows  []*sqlmock.Rows
	Result      sql.Result
	Args        []sqlmock.Argument
}

type Request struct {
	HttpRequest   *http.Request
	Headers       map[string]string
	ParamNames    []string
	ParamValues   []string
	QueryParams   map[string]string
	ContextParams map[string]interface{}
	Validator     echo.Validator
}

func StartDBMock(mocks []Mock) {
	StartDBMockCompleteDSN(mocks, true)
}

func StartDBMockType(mocks []Mock) {
	StartDBMockComplete(mocks, true)
}

func StartDBMockCompleteDSN(mocks []Mock, version bool) {
	dsn := fmt.Sprintf("%s/db", random.String(10))
	var err error
	_, createdMock, err := sqlmock.NewWithDSN(dsn)
	if err != nil {
		panic(err)
	}
	currentMock = createdMock
	viper.SetDefault("DB.Driver", "sqlmock")
	viper.Set("DB.DSN", dsn)
	if version {
		currentMock.ExpectQuery(`SELECT VERSION()`).WillReturnRows(sqlmock.NewRows([]string{"VERSION()"}).AddRow("Mock"))
	}
	for _, t := range Tables {
		currentMock.ExpectExec(fmt.Sprintf("CREATE TABLE `%s(.*)", t)).WillReturnResult(sqlmock.NewResult(0, 0))
	}
	for _, mock := range mocks {
		switch mock.QueryType {
		case "EXEC":
			cmd := currentMock.ExpectExec(mock.Query)
			if nil != mock.Args {
				cmd.WithArgs(mock.Args)
			}
			if mock.ReturnError {
				cmd.WillReturnError(errors.New("test"))
			} else {
				cmd.WillReturnResult(mock.Result)
			}
		case "PREPARE":
			cmd := currentMock.ExpectPrepare(mock.Query)
			if mock.ReturnError {
				cmd.WillReturnError(errors.New("test"))
			} else {
				cmd.WillBeClosed()
			}
		case "QUERY":
			cmd := currentMock.ExpectQuery(mock.Query)
			if nil != mock.Args {
				cmd.WithArgs(mock.Args)
			}
			if mock.ReturnError {
				cmd.WillReturnError(errors.New("test"))
			} else {
				cmd.WillReturnRows(mock.ResultRows...)
			}
		case "BEGIN":
			currentMock.ExpectBegin()
		case "COMMIT":
			currentMock.ExpectCommit()
		case "ROLLBACK":
			currentMock.ExpectRollback()
		}
	}
}

func StartDBMockComplete(mocks []Mock, version bool) *sql.DB {
	var err error
	db, createdMock, err := sqlmock.New()
	if err != nil {
		panic(err)
	}
	currentMock = createdMock
	if version {
		currentMock.ExpectQuery(`SELECT VERSION()`).WillReturnRows(sqlmock.NewRows([]string{"VERSION()"}).AddRow("Mock"))
	}
	for _, t := range Tables {
		currentMock.ExpectExec(fmt.Sprintf("CREATE TABLE `%s(.*)", t)).WillReturnResult(sqlmock.NewResult(0, 0))
	}
	for _, mock := range mocks {
		switch mock.QueryType {
		case "EXEC":
			cmd := currentMock.ExpectExec(mock.Query)
			if nil != mock.Args {
				cmd.WithArgs(mock.Args)
			}
			if mock.ReturnError {
				cmd.WillReturnError(errors.New("test"))
			} else {
				cmd.WillReturnResult(mock.Result)
			}
		case "QUERY":
			cmd := currentMock.ExpectQuery(mock.Query)
			if nil != mock.Args {
				cmd.WithArgs(mock.Args)
			}
			if mock.ReturnError {
				cmd.WillReturnError(errors.New("test"))
			} else {
				cmd.WillReturnRows(mock.ResultRows...)
			}
		case "BEGIN":
			currentMock.ExpectBegin()
		case "COMMIT":
			currentMock.ExpectCommit()
		case "ROLLBACK":
			currentMock.ExpectRollback()
		}
	}
	return db
}

func HTTPRequest(method, path string, queryParams map[string]string, header map[string]string, body io.Reader, e *echo.Echo) (int, string) {
	req := httptest.NewRequest(method, path, body)
	if nil != body {
		req.Header.Set("Content-Type", echo.MIMEApplicationJSON)
	}
	if nil != header {
		for k, v := range header {
			req.Header.Set(k, v)
		}
	}
	if 0 < len(queryParams) {
		q := req.URL.Query()
		for key, val := range queryParams {
			q.Add(key, val)
		}
		req.URL.RawQuery = q.Encode()
	}
	rec := httptest.NewRecorder()
	e.ServeHTTP(rec, req)
	return rec.Code, rec.Body.String()
}

func CreateEchoCtxFromRequest(request Request) echo.Context {
	if nil != request.Headers {
		for k, v := range request.Headers {
			request.HttpRequest.Header.Set(k, v)
		}
	}

	if nil != request.QueryParams {
		q := request.HttpRequest.URL.Query()
		for k, v := range request.QueryParams {
			q.Add(k, v)
		}
		request.HttpRequest.URL.RawQuery = q.Encode()
	}

	e := echo.New()
	if nil != request.Validator {
		e.Validator = request.Validator
	}
	ctx := e.NewContext(request.HttpRequest, httptest.NewRecorder())
	ctx.SetParamNames(request.ParamNames...)
	ctx.SetParamValues(request.ParamValues...)
	if nil != request.ContextParams && 0 < len(request.ContextParams) {
		for k, v := range request.ContextParams {
			ctx.Set(k, v)
		}
	}
	return ctx
}
