module gitlab.com/nikolai.gut/gotestutils

go 1.15

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/labstack/echo/v4 v4.1.17
	github.com/labstack/gommon v0.3.0
	github.com/spf13/viper v1.7.1
)
